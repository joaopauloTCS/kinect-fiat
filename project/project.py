from YOLOmodel import detector_utils as detector_utils
import cv2
import tensorflow as tf
import datetime
import argparse
import winsound
import sys
import numpy as np
import timeit

detection_graph, sess = detector_utils.load_inference_graph()
cam = cv2.VideoCapture(0)

alpha = 0.3

# chroma key
lower = np.array([100,115,115]) 
upper = np.array([110,255,255]) 
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10))

global step
global instruction
step = 0
instruction = "Aperte o botao de inicio"


# area needed to consider the hand to the ROIs [%/100]
overlapArea = 0.0


def superpose(hand ,selection):
    handArea = hand[2] * hand[3]
    
    if hand[0] > selection[0] + selection[2] or hand[0] + hand[2] < selection[0]:
        return False   # does not overlap
    if hand[1] > selection[1] + selection[3] or hand[1] + hand[3] < selection[1]:
        return False   # does not overlap

    min_x = max(hand[0], selection[0])
    max_x = min(hand[0] + hand[2], selection[0] + selection[2])
    min_y = max(hand[1], selection[1])
    max_y = min(hand[1] + hand[3], selection[1] + selection[3])
    intesercArea = (max_x-min_x)*(max_y-min_y)

    if intesercArea/handArea > overlapArea:
        return True
    else:
        return False

def checkRestart(hands, selections):
    global step
    if hands is not None:
        for hand in hands:
            for selection in selections:
                if superpose(hand ,selection): #overlaps!
                    return True
    return False

def checkOverlap(contours, hands, selections):
    global step
    overlaps = 0 # 0 = not overlapping, -1 = wrong overlap, positive integer = correct overlap
    if hands is not None:
        for hand in hands:
            for selection in selections:
                if selection[5] == 'box' or selection[5] == 'boxDone':
                    if superpose(hand ,selection): #overlaps!
                        if selection[4] == step and (selection[5] == 'box' or selection[5] == 'toScrew'):
                            overlaps = step
                            return overlaps
                        else:
                            overlaps = -1

    if contours is not None:
        for cnt in contours:
            for selection in selections:
                if selection[5] != 'box':
                    if insideScrewdDriver(selection, cnt):  # it overlaps!
                        if selection[4] == step and selection[5] == 'toScrew':
                            overlaps = step
                            return overlaps
                        else:
                            overlaps = -1
    return overlaps

def insideScrewdDriver(selection, contour):
    x = int(selection[0])
    while x <= int(selection[0] + selection[2]):
        y = int(selection[1])
        while y <= int(selection[1] + selection[3]):
            epsilon = 0.1*cv2.arcLength(contour,True)
            approx = cv2.approxPolyDP(contour,epsilon,True)
            if cv2.pointPolygonTest(contour,(x,y),False) >= 0:
                return True
            y += 5
        x += 5
    return False

def redMask(img):
    overlay = img.copy()
    cv2.rectangle(overlay, (0, 0), (640, 480), (255, 0, 0), -1)
    cv2.addWeighted(overlay, alpha, img, 1 - alpha, 0, img)
    cv2.putText(img, "PROCEDIMENTO ERRADO!!", (115, 450), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (255, 0, 0), 3)
    return img

def greenMask(img):
    overlay = img.copy()
    cv2.rectangle(overlay, (0, 0), (640, 480), (0, 255, 0), -1)
    cv2.addWeighted(overlay, alpha, img, 1 - alpha, 0, img)
    cv2.putText(img, "Procedimento correto", (115, 450), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 255, 0), 3)
    return img    

def detectChroma(frame):
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower, upper)
    mask = cv2.GaussianBlur(mask, (5, 5), 0)
    mask = cv2.erode(mask, kernel, iterations=1)
    mask = cv2.dilate(mask, kernel, iterations=1)  
    im2, contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        epsilon = 0.1*cv2.arcLength(cnt,True)
        approx = cv2.approxPolyDP(cnt,epsilon,True)
        cv2.drawContours(frame, [cnt], 0, (255,0,0), 2)
    return contours, frame

def detectHands(image_np):
    score_thresh = 0.2
    num_workers = 4
    queue_size = 5
    im_width, im_height = (cam.get(3), cam.get(4))
    # max number of hands we want to detect/track
    num_hands_detect = 2

    image_np = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
    boxes, scores = detector_utils.detect_objects(image_np, detection_graph, sess)

    hands = detector_utils.draw_box_on_image(num_hands_detect, score_thresh,
                                        scores, boxes, im_width, im_height,
                                        image_np)
    return image_np, hands

def detectObjects():
    ret, frame = cam.read()
    contours, frame = detectChroma(frame)
    image_np, hands = detectHands(frame)

    return contours, image_np, hands


def drawROI(image, selections):
    global step
    for selection in selections: 
        if selection[5] == 'restart':
            cv2.rectangle(image, (selection[0], selection[1]), (selection[0] + selection[2], selection[1] + selection[3]), (255,255,255), thickness=3, lineType=8, shift=0)

        elif selection[5] == 'done' or selection[4] < step or selection[5] == 'boxDone':
            cv2.rectangle(image, (selection[0], selection[1]), (selection[0] + selection[2], selection[1] + selection[3]), (0,255,0), thickness=2, lineType=8, shift=0)

            p1 = (int(selection[0] + 0.2*selection[2]), int(selection[1] + 0.6*selection[3]))
            p2 = (int(selection[0] + 0.4*selection[2]), int(selection[1] + 0.8*selection[3]))
            cv2.line(image,p1,p2,(0,255,0),5)

            p1 = (int(selection[0] + 0.4*selection[2]), int(selection[1] + 0.8*selection[3]))
            p2 = (int(selection[0] + 0.8*selection[2]), int(selection[1] + 0.3*selection[3]))
            cv2.line(image,p1,p2,(0,255,0),5)

        elif selection[5] == 'toScrew':
            cv2.rectangle(image, (selection[0], selection[1]), (selection[0] + selection[2], selection[1] + selection[3]), (255,255,50), thickness=2, lineType=8, shift=0)
            radius = int(0.1*selection[2])

            center = (int(selection[0] + 0.2*selection[2]), int(selection[1] + 0.5*selection[3]))
            cv2.circle(image, center, radius, (255,255,50),-1)

            center = (int(selection[0] + 0.5*selection[2]), int(selection[1] + 0.5*selection[3]))
            cv2.circle(image, center, radius, (255,255,50),-1)

            center = (int(selection[0] + 0.8*selection[2]), int(selection[1] + 0.5*selection[3]))
            cv2.circle(image, center, radius, (255,255,50),-1)

        elif selection[5] == 'waitingScrew':
            cv2.rectangle(image, (selection[0], selection[1]), (selection[0] + selection[2], selection[1] + selection[3]), (255,0,0), thickness=2, lineType=8, shift=0)

            p1 = (int(selection[0] + 0.2*selection[2]), int(selection[1] + 0.2*selection[3]))
            p2 = (int(selection[0] + 0.8*selection[2]), int(selection[1] + 0.8*selection[3]))
            cv2.line(image,p1,p2,(255,0,0),5)

            p1 = (int(selection[0] + 0.8*selection[2]), int(selection[1] + 0.2*selection[3]))
            p2 = (int(selection[0] + 0.2*selection[2]), int(selection[1] + 0.8*selection[3]))
            cv2.line(image,p1,p2,(255,0,0),5)

        elif selection[4] == step and selection[5] == 'box':
            cv2.rectangle(image, (selection[0], selection[1]), (selection[0] + selection[2], selection[1] + selection[3]), (0,255,0), thickness=3, lineType=8, shift=0)
        
        elif selection[5] == 'box' and selection[4] != step:
            cv2.rectangle(image, (selection[0], selection[1]), (selection[0] + selection[2], selection[1] + selection[3]), (255,0,0), thickness=3, lineType=8, shift=0)
    
    return image

def showFrame(image, time):
    global instruction

    top = 0
    bottom = 50
    left = 0
    right = 0
    image = cv2.copyMakeBorder(image, top, bottom, left, right, cv2.BORDER_CONSTANT, 0)
    cv2.putText(image, instruction, (20, 515), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255, 255, 255), 2)

    if time is not None:
        time = 'Time: ' + time + 's'
        cv2.putText(image, time, (450, 30), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (255, 255, 255), 2)

    cv2.imshow('Single-Threaded Detection', cv2.cvtColor(image, cv2.COLOR_RGB2BGR))
    if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        exit()

def stillCorrect(contours, hands, selections, numFrames):
    global step
    overlaps = False
    if hands is not None:
        for hand in hands:
            overlaps = False
            for selection in selections:
                if superpose(hand ,selection): #overlaps!
                    if selection[4] == step and (selection[5] == 'box' or selection[5] == 'toScrew'):
                        overlaps = True
                        numFrames = 0  # it does overlap, the number of frames is reseted. We assume that the non overlapping frames were caused by hands miss detections
                        break

    if contours is not None:
        for cnt in contours:
            for selection in selections:
                if insideScrewdDriver(selection, cnt):  # it overlaps!
                    if selection[4] == step and selection[5] == 'toScrew':
                        overlaps = True
                        numFrames = 0  # it does overlap, the number of frames is reseted. We assume that the non overlapping frames were caused by hands miss detections
                        break

    if not overlaps:
        numFrames = numFrames + 1
        if numFrames >=5: # check if it is not overlapping for more than 2 consecutive frames. This is done to avoid hands miss detections
            numFrames = 0
        else:
            overlaps = True
    return overlaps, numFrames


def screwFastening(contours, selection):
    overlaps = False

    for cnt in contours:
        if insideScrewdDriver(selection, cnt):  # it overlaps!
            if selection[4] == step and selection[5] == 'toScrew':
                overlaps = True
                break
    return overlaps

def checkRestartButton(hands, selections):
    restartButton = True
    if checkRestart(hands, [x for x in selections if x[5] == 'restart']):
        while restartButton:
            _, image, hands = detectObjects()
            image = drawROI(image, [x for x in selections if x[5] == 'restart'])
            showFrame(image, None)
            if not checkRestart(hands, [x for x in selections if x[5] == 'restart']):
                return False
    return restartButton


def getRestart(selections):
    global step
    while True:
        numFramesRestart = 0
        _, image, hands = detectObjects()
        image = drawROI(image, [x for x in selections if x[5] == 'restart'])
        showFrame(image, None)
        if not checkRestartButton(hands, selections):
            break

def getWebCam(selections):
    global step
    global instruction

    start = timeit.default_timer()
    step = 1
    instruction = "Pegue os parafusos na caixa marcada de verde"
  
    while True:
        numFramesBox = 0
        numFramesScrew = 0
        numFramesFastening = 0
        contours, image, hands = detectObjects()   
        if not checkRestartButton(hands, selections):
            return
        image = drawROI(image, selections)
        overlap = checkOverlap(contours, hands, selections)
        if overlap > 0: ## Something is inside the correct box of correct screw
            insideSomething = True
            while insideSomething:
                contours, image, hands = detectObjects()  
                if not checkRestartButton(hands, selections):
                    return 
                image = drawROI(image, selections)
                image = greenMask(image)
                showFrame(image, str(round(timeit.default_timer() - start, 1)))

                # bolt box
                boxSelections = [x for x in selections if step == x[4] and 'box' == x[5]]
                inBox, numFramesBox = stillCorrect(None, hands, boxSelections, numFramesBox)
                if not inBox:
                    selections = [[x[0], x[1], x[2], x[3], x[4], 'toScrew'] if (x[4] == step and x[5] == 'waitingScrew') else x for x in selections]
                    selections = [[x[0], x[1], x[2], x[3], x[4], 'boxDone'] if (x[4] == step and x[5] == 'box') else x for x in selections]
                    instruction = "Parafuse os parafusos marcados de amarelo"
                    insideSomething = False
                
                # screwdriver
                i = 0
                for x in selections:
                    if step == x[4] and 'toScrew' == x[5]:
                        notScrewed = screwFastening(contours, x)
                        while notScrewed:
                            contours, image, hands = detectObjects()   
                            if not checkRestartButton(hands, selections):
                                return  
                            image = drawROI(image, selections)
                            image = greenMask(image)
                            showFrame(image, str(round(timeit.default_timer() - start, 1)))
                            fastening, numFramesFastening = stillCorrect(contours, None, [x], numFramesFastening)
                            if not fastening:
                                insideSomething = False
                                selections[i][5] = 'done'
                                if all(x[5] == 'done' or x[5] == 'boxDone' for x in selections if x[4] == step):
                                    step = step + 1
                                    instruction = "Pegue os parafusos na caixa marcada de verde"
                                    if all(x[5] == 'done' or x[5] == 'boxDone' or x[5] == 'restart' for x in selections):
                                        timeTaken = str(round(timeit.default_timer() - start, 1))
                                        instruction = "Montagem concluida em " + timeTaken + "s"
                                        while True:
                                            contours, image, hands = detectObjects()  
                                            image = drawROI(image, selections)
                                            showFrame(image, timeTaken)
                                            if not checkRestartButton(hands, selections):
                                                return 
                                break
                    i = i + 1
                
        elif overlap == -1:
            image = redMask(image)

        showFrame(image, str(round(timeit.default_timer() - start, 1)))


def ROI():
    ret_val, img = cam.read()
    fromCenter = False
    r = cv2.selectROI(img, fromCenter)
    return r
    
def main():
    selections = []
    for i in range(3):
        selections.append(ROI())
        selections[-1] = list(selections[-1])
        selections[-1].append(1)
        if i == 0:
            selections[-1].append('box')
        else:
            selections[-1].append('waitingScrew')

    for i in range(4):
        selections.append(ROI())
        selections[-1] = list(selections[-1])
        selections[-1].append(2)
        if i == 0:
            selections[-1].append('box')
        else:
            selections[-1].append('waitingScrew')

    for i in range(1):
        selections.append(ROI())
        selections[-1] = list(selections[-1])
        selections[-1].append(0)
        selections[-1].append('restart')
    cv2.destroyAllWindows()

    getRestart(selections)
    
    while True:
        getWebCam(selections)


if __name__ == '__main__':
    main()