import cv2
import numpy as np

cam = cv2.VideoCapture(0)
# BLUE
lower = np.array([100,130,130]) 
upper = np.array([110,255,255]) 

kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10))

while True:


    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower, upper)
    mask = cv2.GaussianBlur(mask, (5, 5), 0)
    mask = cv2.erode(mask, kernel, iterations=1)
    mask = cv2.dilate(mask, kernel, iterations=1)  

    im2, contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        epsilon = 0.1*cv2.arcLength(cnt,True)
        approx = cv2.approxPolyDP(cnt,epsilon,True)
        cv2.drawContours(frame, [cnt], 0, (255,0,0), 2)
        print(cv2.pointPolygonTest(cnt,(50,50),False))

    cv2.imshow('maks', frame)
    
    if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        exit()